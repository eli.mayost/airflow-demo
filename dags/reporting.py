
from airflow import DAG
from airflow.operators.dummy import DummyOperator
from airflow.providers.cncf.kubernetes.operators.kubernetes_pod import KubernetesPodOperator
from airflow.kubernetes.secret import Secret
from airflow.utils.dates import days_ago

# Secrets to env vars in pods
cos_endpoint = Secret(
    deploy_type = "env",
    deploy_target = "COS_ENDPOINT",
    secret = "cos-secret",
    key = "cos_endpoint"
)

cos_api_key = Secret(
    deploy_type = "env",
    deploy_target = "COS_API_KEY",
    secret = "cos-secret",
    key = "cos_api_key"
)

cos_bucket = Secret(
    deploy_type = "env",
    deploy_target = "COS_BUCKET",
    secret = "cos-secret",
    key = "cos_bucket"
)

default_args = {
    "owner": "airflow",
    "depends_on_past": False,
    "retries": 0,
}

dag = DAG(
    dag_id="reporting_etl",
    default_args=default_args,
    start_date=days_ago(1),
    schedule_interval=None,
    #schedule_interval="0 1 * * *",
    catchup=False,
    tags=["reporting-etl"],
)

with dag:
    start = DummyOperator(
        task_id = "start"
    )
    
    stop = DummyOperator(
        task_id = "stop"
    )

    generate_report = KubernetesPodOperator(
            namespace               = "airflow-demo",
            name                    = "generate_report",
            task_id                 = "generate_report",
            image                   = "registry.gitlab.com/eli.mayost/airflow-demo",
            startup_timeout_seconds = 120,
            cmds                    = ["Rscript"],
            arguments               = ["/cmt/jobs/generate_report.R"],
            #is_delete_operator_pod = True,
            secrets                 = [
                cos_endpoint,
                cos_bucket,
                cos_api_key
            ]
    )

start >> generate_report >> stop
