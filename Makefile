
include .env

TAG=$(shell git rev-parse --short HEAD)

registry-login:
	@-podman login registry.gitlab.com -u "$(GITLAB_USERNAME)" -p "$(GITLAB_TOKEN)"

build-image:
	@-podman build -t registry.gitlab.com/eli.mayost/airflow-demo:$(TAG) -f k8s/Dockerfile .
	@-podman tag registry.gitlab.com/eli.mayost/airflow-demo:$(TAG) registry.gitlab.com/eli.mayost/airflow-demo:latest

push-image: registry-login
	@-podman push registry.gitlab.com/eli.mayost/airflow-demo:$(TAG)
	@-podman push registry.gitlab.com/eli.mayost/airflow-demo:latest

build-and-push: registry-login build-image push-image

start-cluster:
	minikube start --cpus 4 --memory 16g --driver kvm2

stop-cluster:
	minikube stop

deploy-airflow:
	helm upgrade --install airflow apache-airflow/airflow -n airflow-demo -f k8s/values.yaml --debug

uninstall-airflow:
	helm uninstall airflow -n airflow-demo --debug
