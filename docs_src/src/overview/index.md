
# Overview

We are using the following services:

  - GitLab container registry
  - IBM Cloud Object Storage

<br /><br />
<a class="zoom" href="../overview.png" target="_blank">
  <img :src="$withBase('/overview.png')" alt="overview">
</a>
<br /><br />
